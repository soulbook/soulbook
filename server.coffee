

#
# # SimpleServer
#
# A simple chat server using Socket.IO, Express, and Async.
#
http = require("http")
path = require("path")
async = require("async")
socketio = require("socket.io")
express = require("express")
updater = require("./updater")
#
# ## SimpleServer `SimpleServer(obj)`
#
# Creates a new instance of SimpleServer with the following options:
#  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
#
router = express()
router.use express.json()

server = http.createServer(router)
io = socketio.listen(server)
Kinvey = require("kinvey")
promise = Kinvey.init(
  appKey: "kid_PVehjLya5i"
  appSecret: "0fc4b05dd61149bb86c572e9d027410a"
  masterSecret: "071740ea21d84d86889d2dc9738d08ac"
)
promise.then ((activeUser) ->
  promise = Kinvey.ping()
  promise.then ((response) ->
    console.log "Kinvey Ping Success. Kinvey Service is alive, version: " + response.version + ", response: " + response.kinvey
  ), (error) ->
    console.log "Kinvey Ping Failed. Response: " + error.description
), (error) ->

#router.post "/updater", (req, res) ->
  #updater.updateUser req.body.username, (err, result)->
    #res.send result
#
#router.post "/matcher", (req, res) ->
  #updater.updateUser req.body.username, (err, result)->
    #res.send result
inprogress = false
progress = 0
router.get "/updater/status", (req, res) ->
  if inprogress
    res.send 
      status: "inprogress"
      progress: (+progress).toFixed()
    return
  res.send 
    status: "idle"

router.get "/updater/start", (req, res) ->
  if inprogress
    res.send 400
    return
  inprogress = true
  res.send 200
  updater.update (_progress)->
    progress = _progress
    if progress == 100
      inprogress = false
      progress = 0
    console.log progress

router.get "/reminder", (req, res) ->
  
  #Get all tomorrow meetings
  #Hash it by users
  #Check if the email for each user has been sent from the email sent time
  #Send each user a digest email
  #If success, save the last email sent time
  #Otherwise, try again
  #If all email has been sent, respond to the caller OK
  #Otherwise "try again"
  res.send "OK"

router.use express.static(path.resolve(__dirname, "client"))
errorHandler = require("express-error-handler")
handler = errorHandler(static:
  404: "./client/404.html"
)
router.use errorHandler.httpError(404)

# Handle all unhandled errors:
router.use handler

# router.get("/*", function(req,res){
#     //Get all tomorrow meetings
#     //Hash it by users
#     //Check if the email for each user has been sent from the email sent time
#     //Send each user a digest email
#     //If success, save the last email sent time
#     //Otherwise, try again
#     //If all email has been sent, respond to the caller OK
#     //Otherwise "try again"
#     res.redirect('/404.html');
# });
messages = []
sockets = []
io.on "connection", (socket) ->
  socket.on "subscribe", (data) ->
    socket.join data.room

  socket.on "unsubscribe", (data) ->
    socket.leave data.room

  socket.on "message", (data) ->
    socket.broadcast.to(data.room).emit data.event, data #emit to 'room' except this socket



# messages.forEach(function(data) {
#     socket.emit('message', data);
# });

# sockets.push(socket);

# socket.on('disconnect', function() {
#     sockets.splice(sockets.indexOf(socket), 1);
#     updateRoster();
# });

# socket.on('message', function(msg) {
#     var text = String(msg || '');

#     if (!text) return;

#     socket.get('name', function(err, name) {
#         var data = {
#             name: name,
#             text: text
#         };

#         broadcast('message', data);
#         messages.push(data);
#     });
# });

# socket.on('identify', function(name) {
#     socket.set('name', String(name || 'Anonymous'), function(err) {
#         updateRoster();
#     });
# });

# function updateRoster() {
#     async.map(
#     sockets,

#     function(socket, callback) {
#         socket.get('name', callback);
#     },

#     function(err, names) {
#         broadcast('roster', names);
#     });
# }

# function broadcast(event, data) {
#     sockets.forEach(function(socket) {
#         socket.emit(event, data);
#     });
# }
if process.env.IP
  server.listen process.env.PORT or 3000, process.env.IP or "0.0.0.0", ->
    addr = server.address()
    console.log "Chat server listening at", addr.address + ":" + addr.port

else
  server.listen process.env.PORT or 3000

# setTimeout ->  
#   updater.update (_progress)=>
#     console.log "_progress", _progress
# ,1000