Kinvey = require "kinvey"
processor = require "./processor"
store = require "./store"
async = require "async"
Kinvey = require("kinvey")
promise = Kinvey.init
  appKey: "kid_PVehjLya5i"
  appSecret: "0fc4b05dd61149bb86c572e9d027410a"
  masterSecret: "071740ea21d84d86889d2dc9738d08ac"

promise.then (activeUser) ->
  # setTimeout ->
  #   updater = new Updater()
  #   updater.update()
  # , 3000

class Updater
  constructor: ->
  findMatch: (user, cb=->)->
    store.findMatch user.username, (err, matches)->
      async.eachSeries matches, (match, cb)->
        query = new Kinvey.Query()
        query.equalTo "username", match.username
        Kinvey.User.find query,
          success: (response)->
            if not (response and response.length > 0)
              cb()
              return
            response[0].score = match.score
            match = response[0]
            Kinvey.DataStore.update "matches",
              _id: "#{user.username}-#{match.username}"
              score: match.score
              username: user.username
              status: user.status
              match: match.username
              matchStatus: match.status
              matchEvents: match.events
            ,
              success: =>
                cb()
                # console.log arguments...
              error: =>
                cb()
                # console.log arguments...
          error: ->
            cb()
      , ->
        cb()
  updateUser: (username, cb=->)->
    #for one user
    query = new Kinvey.Query()
    query.equalTo "username", username
    Kinvey.User.find query,
      success: (users) =>
        async.eachSeries users, (user, cb)=>
          if user.status
            processor.process user.status, (status_tokens)->
              #console.log "status_tokens", status_tokens
              async.eachSeries status_tokens, (token, cb)=>
                #console.log "user.username", user.username
                #console.log "token", token
                store.insert user.username, token, ->
                  cb()
              , =>
                cb()
          else
            cb()
        , =>
          #console.log "done"
          async.eachSeries users, (user, cb)=>
            #console.log "user.username", user.username              
            if user.status
              @findMatch user.username, ->
                cb()
            else
              cb()
          , =>
            cb()   
  update: (progress=->)->
    query = new Kinvey.Query()
    promise = Kinvey.User.find query,
      success: (users) =>
        prev = 0
        total = users.length
        count = 0
        async.eachSeries users, (user, cb)=>
          if user.status
            processor.process user.status, (status_tokens)->
              #console.log "status_tokens", status_tokens
              async.eachSeries status_tokens, (token, cb)=>
                #console.log "user.username", user.username
                #console.log "token", token
                store.insert user.username, token, ->
                  cb()
              , =>
                prev = (++count/total*50)
                progress prev
                cb()
          else
            prev = (++count/total*50)
            progress prev
            cb()
        , =>
          #console.log "done"
          total = users.length
          count = 0
          async.eachSeries users, (user, cb)=>
            #console.log "user.username", user.username              
            if user.status
              @findMatch user, ->
                progress(prev+(++count/total*50))
                cb()
            else
              progress(prev+(++count/total*50))
              cb()
          , =>
            console.log "done"
            # progress(100)

    #get user from kinvey
    #process user.status  processor.process(status) -> array of tokens
    #put it in store store.insert(username, token)

module.exports = new Updater()