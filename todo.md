Todo
====================

- Find a site domain

- Soul meetings page (sorted by time in each category): 
    - Paging
    - has ability to reschedule
    - exchange conversation to specify place and time
    - send emails reminder to both parties
    - Flash cancel notification (disappear once user click ok)
    - Flash accept notification (disappear once user click ok)
    - Flash decline notification (disappear once user click ok)

- Analytics
- Contact form
- Notification digest via email, a day before the meeting (cron job)

(Features)
-customize the confirmation page on Kinvey (tuan, ha, phi)
-password reset (tuan, ha, phi)
-all other issues that are or will be on bitbuckets (tuan, ha, phi)
-change to profile picture (instead of 150x150 picture) (tuan, ha, phi)
-contact form  (surat)
-email notification  (surat)
-analytics (with flurry, segment.io)  (surat)
(Quality Assurance)  
-bugs fix (tuan, ha, phi)
-loading speed (minify, upgrade heroku) (surat)
-scalability test (traffic test) (surat)
(Logistic) 
-Google apps (for email): support@*.com (surat)
-Domain name: *.com (surat)
(Legal) 
-Privacy page (tuan,ha)
-Terms page (tuan,ha)
(Marketing) 
-facebook account (tuan,ha)
-twitter account (tuan,ha)
-google+ account (tuan,ha)
