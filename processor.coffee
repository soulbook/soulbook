natural = require('natural')
metaphone = natural.Metaphone.process
stem = natural.PorterStemmer.stem
normalizer = require "./normalizer.js"
_ = require "lodash"
_.mixin capitalize: (string) ->
  string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
Tokenizer = natural.AggressiveTokenizer
tokenizer = new Tokenizer()
wordnet = new natural.WordNet()
async = require("async")
stopwords = natural.stopwords

class Processor
  constructor: ->
  addSynonym:(words, cb) ->
  #add synonym and remove verbs
    resulting_words = {}
    
    #remove words that are not noun
    async.each words, ((w, cb) ->
      wordnet.lookup w, (different_meanings) ->
        
        # console.log(different_meanings);
        different_meanings.every (e) ->
          
          # console.log(e);
          if e.pos is "n"
            if w.match /[\.,-\/#!$%\^&\*;:{}=\-_`~\(\)]/g
              return true
            resulting_words[w] = 1
            
            #console.log("resulting_words",resulting_words);
            # e.synonyms.forEach(function(syn){
            #   resulting_words.push(syn);
            # });
            false
          else
            true
        #add synonyms
        tmpwords = Object.keys(resulting_words)
        async.each tmpwords, ((tmpword, cb) ->
          wordnet.lookup tmpword, (different_meanings) ->
            for different_meaning in different_meanings
              for syn in different_meaning.synonyms
                if not syn.match(/[\.,-\/#!$%\^&\*;:{}=\-_`~\(\)]/g)
                  resulting_words[syn] = true
            cb()
        ), ->
          cb()
    ), ->
      cb null, Object.keys(resulting_words)
  
  process: (sentence, cb)->
    #tokenize
    words = tokenizer.tokenize(sentence)
    #normalize
    words = normalizer.normalize_tokens(words)
    #remove Capital words
    # cap = []
    # words = words.filter (word)->  
    #   if word == _.capitalize(word) and word.length > 1 #capital word
    #     cap.push word
    #     return false
    #   return true
    #lowercase
    words = words.map (word)->
      word.toLowerCase()

    #remove stopwords
    words = words.filter (word)->
      stopwords.indexOf(word) == -1

    #add synonym and remove verbs
    @addSynonym words, (err, words)=>
  
      #stem
      words = words.map (word)->
        stem word
      console.log words
      
      #metaphone
      words = @metaphoneArray(words)
      console.log words
      
      #add back Capital words
      # words = _.union words, cap
      # console.log words
      cb words

  metaphoneArray: (words) ->
    arr = []
    constant = undefined
    return arr  unless words
    i = 0
    len = words.length

    while i < len
      constant = metaphone(words[i])
      arr.push constant  unless ~arr.indexOf(constant)
      ++i
    arr
  
module.exports = new Processor()
# module.exports.addSynonym ["hello","run"], ->
#   console.log arguments...
