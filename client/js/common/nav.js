function NavController($scope) {
    var decrement = function() {
        $scope.numberOfInvites--;
        if ($scope.numberOfInvites === 0) {
            $scope.numberOfInvites = ""
        }
    };
    $scope.numberOfInvites = "";
    var user = Kinvey.getActiveUser();
    var query = new Kinvey.Query();
    query.notEqualTo('status', "accept").and().notEqualTo('status', "decline");
    query.equalTo('to.username', $scope.user.username);
    query.greaterThanOrEqualTo('startUnixTime', moment().unix());
    query.ascending('startUnixTime');

    // Sign out from the session
    $scope.signOut = function() {
      var user = Kinvey.getActiveUser();
      if (null !== user) {
        Kinvey.User.logout({
          success: function() {
            window.location.pathname = "/";
          },
          error: function() {
            window.location.pathname = "/";
          }
        });
      }
    }

    $scope.$on("accept", function() {
        decrement();
    });
    $scope.$on("decline", function() {
        decrement();
    });

    socket.on("delete", function(data) {
        for (var i in $scope.meetings) {
            var meeting = $scope.meetings[i];
            if (meeting._id === data._id) {
                decrement();
                $scope.$apply();
                break;
            }
        }
    });
    socket.on("invite", function(data) {
        $scope.numberOfInvites++;
        $scope.$apply();
    });

    Kinvey.DataStore.count("meetings", query, {
        success: function(response) {
            if (response === 0) {
                $scope.numberOfInvites = ""
            }
            else {
                $scope.numberOfInvites = response
            }
            $scope.$apply();
        },
        error: function() {

        }
    });
}