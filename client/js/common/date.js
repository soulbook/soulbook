// Variables
var daysOfTheWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri","Sat"]; 


// Functions

var updateToActualDate = function(event) {
    var now = moment();
    var weeknow = moment().isoWeek();
    var start = moment(event.start);
    var diff = weeknow - start.isoWeek();
    if (now.isoWeekday() > start.isoWeekday()) {
      diff++;
    }
    start = start.add('w', diff);
    var end = moment(event.end);
    diff = weeknow - end.isoWeek();
    if (now.isoWeekday() > end.isoWeekday()) {
      diff++;
    }
    end = end.add('w', diff);
    //console.log(diff);
    //console.log(end.toDate());
    
    event.start = start.toDate();
    event.end = end.toDate();
}

// Return a list of 7 days starting from today. e.g.: if today is Wed, then it returns ['Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Mon', 'Tue']
var getNext7Days = function() {
    
    var date = new Date();
    var curIdx = date.getDay();
    
    var firstpart = daysOfTheWeek.slice(0, curIdx);
    var secondpart = daysOfTheWeek.slice(curIdx, daysOfTheWeek.length);

    return secondpart.concat(firstpart);
}