function FeedbackController($scope) {
  var user = Kinvey.getActiveUser();
  $scope.sendFeedback = function() {
    $scope.feedback.user = user.username;

    Kinvey.DataStore.save("feedbacks", angular.copy($scope.feedback), {
      success: function(response) {
        $scope.feedbackStatus = 'sent';
        $scope.$apply();
        // wait 1.5 sec then dismiss feedback form
        setTimeout(function() {
          $scope.feedbackOn = false;
          $scope.feedback.description = "";
          $scope.feedbackStatus = '';
          $scope.$apply();
        }, 1500);
      },
      error: function(error) {
        $scope.feedbackStatus = '';
      }
    });
  }
}
