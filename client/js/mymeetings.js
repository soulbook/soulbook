var promise = Kinvey.init({
    appKey: 'kid_PVehjLya5i',
    appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});
window.socket = io.connect();

promise.then(function() {

    // Attempt log in
    var user = Kinvey.getActiveUser();
    if (user === null) {
        window.location.pathname = "/signin.html";
    }

    var app = angular.module('app', ['ui.calendar']);
    window.app = app;

    app.controller("MeetingController", function($scope,$timeout) {

        // INITIALIZE -----------  

        // Initialize variables
        $scope.user = user;


        // Load all related meetings
        var query = new Kinvey.Query();
        query.equalTo('from._id', $scope.user._id).or().equalTo('to._id', $scope.user._id);
        query.greaterThanOrEqualTo('startUnixTime', moment().subtract('m',60).unix());
        query.ascending('startUnixTime');

        var promise = Kinvey.DataStore.find("meetings", query, {

            success: function(response) {

                $scope.allMeetings = response;
                updateUserInfo($scope.allMeetings);

                // Filter declined meetings
                $scope.declinedmeetings = $scope.allMeetings.filter(function(meeting) {
                    return meeting.status == 'decline' && meeting.to._id == $scope.user._id;
                });

                // Filter pending invitations
                $scope.sentmeetings = $scope.allMeetings.filter(function(meeting) {
                    return meeting.status != 'accept' && meeting.status != 'decline' && meeting.from._id == $scope.user._id;
                });

                // Filter incoming invitations
                $scope.meetings = $scope.allMeetings.filter(function(meeting) {
                    return meeting.status != 'accept' && meeting.status != 'decline' && meeting.to._id == $scope.user._id;
                });

                // Filter confirmed meetings
                $scope.confirmedmeetings = $scope.allMeetings.filter(function(meeting) {
                    return meeting.status == 'accept';
                });

                // console.log($scope.confirmedmeetings);
                // Obtain event schedule
                $scope.confirmedmeetings.forEach(function(meeting) {
                    meeting.event.meeting = meeting;
                    $scope.confirmedevents.push(meeting.event);
                    // console.log($scope.confirmedevents);
                    meeting.event.color = "purple";
                    meeting.event.start = moment(meeting.event.start).toDate();
                    meeting.event.end = moment(meeting.event.end).toDate();
                })

                // Update UI
                $scope.$apply();
            }
        });

        // SOCKET ------------------

        socket.emit('subscribe', {
            room: user.username
        });
        socket.on("accept", function(data) {
            // //console.log("accept");
            // //console.log(data);
            for (var i in $scope.sentmeetings) {
                var meeting = $scope.sentmeetings[i];
                if (meeting._id === data._id) {
                    //console.log("meeting.status: " + meeting.status);
                    meeting.status = "accept";
                    var mtg = $scope.sentmeetings.splice(i, 1);
                    $scope.confirmedmeetings.push(meeting);
                    $scope.confirmedevents.push(meeting.event);
                    meeting.event.color = "purple";
                    meeting.event.start = moment(meeting.event.start).toDate();
                    meeting.event.end = moment(meeting.event.end).toDate();
                    $scope.$apply();
                    break;
                }
            }
            //SEGMENT.IO TRACKING CODE
            analytics.track('Meeting-ClickAccept', {
                event: 'Click'
            });
        });
        socket.on("decline", function(data) {
            //console.log("decline");
            //console.log(data);
            for (var i in $scope.sentmeetings) {
                var meeting = $scope.sentmeetings[i];
                if (meeting._id === data._id) {
                    //console.log("meeting.status: " + meeting.status);
                    meeting.status = "decline";
                    $scope.$apply();
                    break;
                }
            }
            //SEGMENT.IO TRACKING CODE
            analytics.track('Meeting-ClickDecline', {
                event: 'Click'
            });
        });
        socket.on("invite", function(data) {
            //console.log("invite");
            //console.log(data);
            $scope.meetings.push(data.meeting);
            $scope.$apply();
            //SEGMENT.IO TRACKING CODE
            analytics.track('Meeting-ClickInvite', {
                event: 'Click'
            });
        });
        socket.on("delete", function(data) {
            //console.log("delete");
            //console.log(data);

            for (var i in $scope.meetings) {
                var meeting = $scope.meetings[i];
                if (meeting._id === data._id) {
                    $scope.meetings.splice(i, 1);
                    $scope.$apply();
                    break;
                }
            }

            for (var i in $scope.declinemeetings) {
                var meeting = $scope.declinemeetings[i];
                if (meeting._id === data._id) {
                    $scope.declinemeetings.splice(i, 1);
                    $scope.$apply();
                    break;
                }
            }

            for (var i in $scope.confirmedmeetings) {
                var meeting = $scope.confirmedmeetings[i];
                if (meeting._id === data._id) {
                    //console.log("REMOVE event");
                    //console.log($scope.confirmedevents);

                    $scope.confirmedmeetings.splice(i, 1);
                    $scope.confirmedevents.splice(i, 1);

                    $scope.$apply();
                    break;
                }
            }

            //SEGMENT.IO TRACKING CODE
            analytics.track('Meeting-ClickDelete', {
                event: 'Click'
            });
        });

        // CALENDAR STUFF ------------------------------------------------------------------------------------------

        $scope.weekdays = getNext7Days();


        var updateUserInfo = function(meetings, cb) {

            meetings.forEach(function(meeting) {
                var query = new Kinvey.Query();
                query.equalTo('_id', meeting.to._id);
                Kinvey.User.find(query, {
                    success: function(resp) {
                        if (resp && resp[0]) {
                            //console.log(resp[0].picture)
                            meeting.to = resp[0];
                            $scope.$apply();
                        }
                    },
                    error: function(resp) {
                        //console.log(arguments);
                    }
                });
                var query = new Kinvey.Query();
                query.equalTo('_id', meeting.from._id);
                Kinvey.User.find(query, {
                    success: function(resp) {
                        if (resp && resp[0]) {
                            //console.log(resp[0].picture)
                            meeting.from = resp[0];
                            $scope.$apply();
                        }
                    },
                    error: function(resp) {
                        //console.log(arguments);
                    }
                });
            });
        }




        $scope.checkConflicts = function() {
            $scope.meetings.forEach(function(meeting) {
                $scope.checkConflict(meeting);
            });
        };

        $scope.checkConflict = function(meeting) {
            var query = new Kinvey.Query();
            query.greaterThanOrEqualTo('startUnixTime', moment(meeting.event.start).unix()).and().lessThanOrEqualTo('startUnixTime', moment(meeting.event.end).unix());
            query.equalTo("to._id", $scope.user._id);
            query.equalTo('status', "accept");
            query.ascending('startUnixTime');
            Kinvey.DataStore.count("meetings", query, {
                success: function(count) {
                    if (count > 0) {
                        meeting.warnmessage = "You have already scheduled a meeting at this time. ";
                    }
                    else {
                        meeting.warnmessage = "";
                    }
                    $scope.$apply();
                },
                error: function() {

                }
            });
        };

        $scope.accept = function(meeting) {
            meeting.status = "accept";

            for (var i in $scope.meetings) {
                var inmeeting = $scope.meetings[i];
                if (meeting._id === inmeeting._id) {
                    $scope.meetings.splice(i, 1);
                    break;
                }
            }
            $scope.checkConflicts();

            $scope.confirmedmeetings.push(meeting);
            $scope.confirmedevents.push(meeting.event);
            meeting.event.color = "purple";
            meeting.event.start = moment(meeting.event.start).toDate();
            meeting.event.end = moment(meeting.event.end).toDate();

            Kinvey.DataStore.update("meetings", angular.copy(meeting), {
                success: function() {
                    $scope.$broadcast("accept");
                    socket.emit("message", {
                        room: meeting.from.username,
                        event: "accept",
                        _id: meeting._id
                    });
                },
                error: function() {

                }
            });
        }
        $scope.decline = function(meeting) {
            meeting.status = "decline";
            $scope.declinemeetings.push(meeting);
            $scope.meetings = $scope.meetings.filter(function(meeting) {
                return meeting.status !== "decline";
            });

            Kinvey.DataStore.update("meetings", angular.copy(meeting), {
                success: function() {
                    $scope.$broadcast("decline");
                    socket.emit("message", {
                        room: meeting.from.username,
                        event: "decline",
                        _id: meeting._id
                    });
                },
                error: function() {

                }
            });
        }

        $scope.reschedule = function(meeting) {
            meeting.status = "reschedule";
            Kinvey.DataStore.update("meetings", meeting, {
                success: function() {

                },
                error: function() {

                }
            });
        }

        $scope.cancel = function(meeting, index) {
            meeting.status = "decline";

            Kinvey.DataStore.destroy("meetings", meeting._id, {
                success: function() {
                    $('.carousel').one('slid.bs.carousel', function() {
                        $scope.confirmedmeetings.splice(index, 1);
                        $scope.confirmedevents.splice(index, 1);
                        $scope.$apply();
                    });
                    if (index >= 1) {
                        $('.carousel').carousel(index - 1);
                    }
                    else {
                        $scope.confirmedmeetings.splice(index, 1);
                        $scope.confirmedevents.splice(index, 1);
                        $scope.$apply();
                    }


                    socket.emit("message", {
                        room: meeting.to.username,
                        event: "delete",
                        _id: meeting._id
                    });

                },
                error: function() {

                }
            });

        }

        $scope.del = function(meeting, index) {
            meeting.status = "decline";

            Kinvey.DataStore.destroy("meetings", meeting._id, {
                success: function() {
                    $scope.sentmeetings.splice(index, 1);
                    socket.emit("message", {
                        room: meeting.to.username,
                        event: "delete",
                        _id: meeting._id
                    });
                    $scope.$apply();
                },
                error: function() {

                }
            });

        }

        $scope.cancelMeeting = function() {
            //TO DO
        }

        $scope.dayChosen = function(arg) {
            $scope.dayChosen = arg; //arg corresponds to the a location in the weekday array
        }

        // Reschedule the meeting by sending a return invitation
        $scope.sendInvitation = function(invitedUser, day, time) {
            $scope.newMeeting = {};
            $scope.newMeeting.from = user;
            $scope.newMeeting.to = invitedUser;
            $scope.newMeeting.day = day;
            $scope.newMeeting.time = time;

            $('#myModal').modal({});
        }

        // send meeting data to kinvey datastore : in "meetings" database
        $scope.sendMessage = function() {

            $scope.newMeeting.status = "sent";

            var acl = new Kinvey.Acl($scope.newMeeting);
            // Add read permissions to user “John Doe” with _id “johndoe”.
            acl.addReader($scope.newMeeting.from._id);
            acl.addWriter($scope.newMeeting.from._id);
            acl.addReader($scope.newMeeting.to._id);
            acl.addWriter($scope.newMeeting.to._id);

            Kinvey.DataStore.save("meetings", angular.copy($scope.newMeeting), {
                success: function(data) {
                    $('#myModal').modal('hide');
                    $scope.sentmeetings.splice(0, 0, data);
                    $scope.$apply();
                    //console.log(arguments);
                },
                error: function(error) {
                    $('#myModal').modal('hide');
                    alert(error.name);

                    //console.log(arguments);
                }
            });
        }

        //
        // send meeting data to kinvey datastore : in "meetings" database
        $scope.editNote = function(meeting) {
            //console.log("click");
            meeting.showEditNote = true;
            setTimeout(function() {
                $("#noteinput").focus();
            }, 0);
        }


        $scope.updateNote = function(meeting) {
            meeting.showEditNote = !meeting.showEditNote;
            Kinvey.DataStore.save("meetings", angular.copy(meeting), {
                success: function(data) {},
                error: function(error) {}
            });
        }

        $scope.initChat = function(meeting) {
            meeting.myDataRef = new Firebase('https://chatsey.firebaseio.com/' + meeting._id);
            meeting.chatLog = [];
            meeting.myDataRef.on('child_added', function(snapshot) {
                var message = snapshot.val();
                $timeout(function(){
                  meeting.chatLog.push(message);
                });
                // $scope.$apply();
            });
        }
        $scope.fireChat = function(event, meeting) {
            if (event.keyCode === 13) {
                var name = $scope.user.username;

                meeting.myDataRef.push({
                    name: name,
                    text: meeting.chatText
                });

                meeting.chatText = "";
            }
        }

        // Calendar stuff

        /* config object */
        $scope.uiConfig = {
            calendar: {
                editable: false,
                header: {
                    left: 'agendaWeek,agendaDay',
                    center: '',
                    right: 'prev,next'
                },
                columnFormat: {
                    week: 'ddd MM.dd',
                },
                defaultView: 'agendaWeek',
                dayClick: function(event) {

                },
                eventDrop: function(event) {

                },
                eventResize: function(event) {

                },
                eventClick: function(event) {
                    $scope.confirmedmeetings.forEach(function(meeting, index) {
                        if (meeting._id === event.meeting._id) {
                            $('.carousel').carousel(index);
                        }
                    });
                    // var startTime = moment(event.start);
                    // var endTime = moment(event.end);
                    // $scope.currentInfo = {
                    //   day: startTime.format("ddd"),
                    //   date: startTime.format("MMM, DD"),
                    //   time: {
                    //     from: startTime.format("hh:mma"),
                    //     to: endTime.format("hh:mma")
                    //   },
                    //   place: event.title || event.place,
                    //   meeting: event.meeting
                    // };
                    // $('#infoModal').modal({});

                    // $scope.$apply();
                },
                minTime: 11,
                maxTime: 14,
                // firstHour: 10,
                // height: 250,
                allDaySlot: false,
                slotEventOverlap: false,
                disableDragging: true,
                disableResizing: true,
                firstDay: (new Date()).getDay()
            }
            // <<<<<<< HEAD
        };
        // );
        // var startTime = moment(event.start);
        // var endTime = moment(event.end);
        // $scope.currentInfo = {
        //   day: startTime.format("ddd"),
        //   date: startTime.format("MMM, DD"),
        //   time: {
        //     from: startTime.format("hh:mma"),
        //     to: endTime.format("hh:mma")
        //   },
        //   place: event.title || event.place,
        //   meeting: event.meeting
        // };
        // $('#infoModal').modal({});

        // $scope.$apply();
        //   },
        //   minTime: 11,
        //   maxTime: 14,
        //   // firstHour: 10,
        //   // height: 250,
        //   allDaySlot: false,
        //   slotEventOverlap: false,
        //   disableDragging: true,
        //   disableResizing: true,
        //   firstDay: (new Date()).getDay()
        // }
        // };

        if ($scope.confirmedevents === undefined) {
            $scope.confirmedevents = [];
        }
        // =======
        //         };

        //         if ($scope.confirmedevents === undefined) {
        //             $scope.confirmedevents = [];
        //         }
        // >>>>>>> 009cdf365afc266e3f9b7febbdf7e2c8519525d1

        /* event sources array*/
        $scope.eventSources = [$scope.confirmedevents];
        console.log($scope.confirmedevents);
        // console.log($scope.uiConfig.calendar);

    });
    angular.bootstrap(document, ['app']);

    // var user = Kinvey.getActiveUser();
    // //console.log(user);
    // if(user==null){
    //     window.location.pathname = "./signin.html"
    // }
});
