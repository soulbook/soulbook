// Offline -------------------------

var validateUsername = function(username) {
    
    if (username == null || username == "") return "Please enter a username.";

    return null;
}

var validatePassword = function(password) {

    if (password == null || password == "") return "Please enter a password.";

    if (password.length < 7) return "Password must be at least 7 characters long.";

    return null;
}

var validateEmail = function(email) {

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        return "Invalid email address";
    }

    // mit.edu email addresses only
    var domain = email.match(/(@[a-zA-Z.\-\_]+)/);
    // if (domain[0] != "@mit.edu")
    // {
    //     return "Only @mit.edu email addresses are allowed.";
    // }

    return null;
}

var validatePhoneNumber = function(phone) {
    
    if (phone == null || phone == "") return null;
    
    var filter = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (!filter.test(phone)) {
        return "Invalid phone number";
    }
    return null;
}

// Online ------------------------------------