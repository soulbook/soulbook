var promise = Kinvey.init({
  appKey: 'kid_PVehjLya5i',
  appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});
promise.then(function() {

  function addDate(date, day) {
    return new Date(date.getTime() + 86400000);
  }

  var app = angular.module('app', []);
  window.app = app;
  //console.log("app: ", app);

  app.controller("SignInController", function($scope) {
    var user = null;
    var promise = Kinvey.User.me({
      success: function(response) {
        //console.log("success");
      },
      error: function(response) {
        //console.log("error");
        var promise = Kinvey.User.logout({
          force: true,
          success: function() {
            window.location.pathname = "/signin.html";
          }
        });
      }
    });

    user = Kinvey.getActiveUser();
    if (user !== null) {
      //console.log(user);
      var metadata = new Kinvey.Metadata(user);
      var status = metadata.getEmailVerification();
      if (status === 'confirmed') {
        window.location.pathname = "/mainpage.html";
      }
    }
    else {
      var promise = Kinvey.User.logout({
        force: true,
        success: function() {
          window.location.pathname = "/signin.html";
        }
      });
    }

    var socket = io.connect();
    $scope.user = {};
    $scope.user.username = '';
    $scope.user.password = '';


    $scope.message = null;
    $scope.sendSignInInfo = function() {
      // window.location.pathname = "/mainpage.html"
      Kinvey.User.login($scope.user.username, $scope.user.password, {
        success: function(response) {
          var user = Kinvey.getActiveUser();
          var metadata = new Kinvey.Metadata(user);
          var status = metadata.getEmailVerification();

          if (status === "confirmed") {
            if (response.status === "filled") {
              window.location.pathname = "/mainpage.html"
            }
            else {
              window.location.pathname = "/profile.html"
            }
          }
          else {
            $scope.message = "Please check your email to verify your email address."
            var promise = Kinvey.User.logout({
              force: true,
              success: function() {}
            });
          }
          $scope.$apply();
        },

        //Send eror mesage when there is sign-in error
        error: function(error) {
          $scope.message = "Please check your username and/or password";
          if (error.name === "EmailVerificationRequired") {
            $scope.message = "Please check your email to verify your email address."
          }
          //console.log(arguments);
          $scope.$apply();
        }
      });
    }

    $scope.signOut = function() {
      //console.log("logout");
      var user = Kinvey.getActiveUser();
      if (null !== user) {
        var promise = Kinvey.User.logout({
          success: function() {
            window.location.pathname = "/";
          },
          error: function() {
            window.location.pathname = "/";
          }
        });
      }
    }

  });
  angular.bootstrap(document, ['app']);

  // var user = Kinvey.getActiveUser();
  // //console.log(user);
  // if(user==null){
  //     window.location.pathname = "./signin.html"
  // }
});
