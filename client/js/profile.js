var promise = Kinvey.init({
    appKey: 'kid_PVehjLya5i',
    appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});
window.socket = io.connect();

// FilePicker Setup
filepicker.setKey("ABd1mUTTQTruYguwQIl1Yz");

promise.then(function() {

    var user = Kinvey.getActiveUser();
    if (user === null) {
        window.location.pathname = "/signin.html";
    }


    var app = angular.module('app', ['ui.calendar']);
    window.app = app;

    if (NavController) {
        app.controller("NavController", NavController);
    }


    function addDate(date, day) {
        return new Date(date.getTime() + 86400000);
    }

    var app = angular.module('app', ['ui.calendar']);
    window.app = app;

    if (NavController) {
        app.controller("NavController", NavController);
    }

    //console.log("app: ", app);

    var updateToActualDate = function(event) {
        var now = moment();
        var day = now.day();
        var start = moment(event.start);
        start.year(now.year());
        start.month(now.month());
        start.date(now.date());
        if (start.day() > moment(event.start).day()) {
            start.day(moment(event.start).day());
            start = start.add('w', 1);
        }
        else {
            start.day(moment(event.start).day());
        }
        // start.date(now.date()+(start.day()-day));
        // console.log("start.day()",start.day());
        event.start = start.toDate();
        var end = moment(event.end);
        end.year(now.year());
        end.month(now.month());
        end.date(now.date());
        if (end.day() > moment(event.end).day()) {
            end.day(moment(event.end).day());
            end = end.add('w', 1);
        }
        else {
            end.day(moment(event.end).day());
        }
        // end.date(now.date()+(end.day()-day));
        end.day(moment(event.end).day());
        event.end = end.toDate();

    }

    app.controller("ProfileController", function($scope, $timeout) {
        // var date = new Date();
        // var curIdx = date.getDay();

        $scope.addEvent = function(start, end, allDay) {

            var m = 1;
            var y = 2009;

            var startMoment = moment(new Date(y, m));
            startMoment.day(start.getDay());
            startMoment.hour(start.getHours());
            startMoment.minute(start.getMinutes());

            var endMoment = moment(new Date(y, m));
            endMoment.day(end.getDay());
            endMoment.hour(end.getHours());
            endMoment.minute(end.getMinutes());

            //console.log("d", d);
            //console.log(h);
            var start = startMoment.toDate();
            var end = endMoment.toDate();
            var event = {
                title: 'MIT Campus',
                start: start,
                end: end,
                startUnixTime: Math.floor(start.getTime() / 1000),
                endUnixTime: Math.floor(end.getTime() / 1000),
                allDay: allDay
            };
            console.log("start.getDay()", start.getDay());
            console.log("end.getDay()", end.getDay());

            updateToActualDate(event);
            $scope.events.push(event);
            return event;
        };

        
        $scope.tmp = {};
        $scope.renderEvent = function(start, end) {
            var s = moment(start);
            var e = moment(end);
            if (!$scope.tmp.dateRendered) {
                $scope.tmp.dateRendered = {};

                $scope.user.events.map(function(event) {
                    $scope.tmp.dateRendered[moment(event.start).format("MM-DD-YYYY")] = true;
                });
            }
            if (!$scope.tmp.eventMap || $scope.tmp.eventMap.length === 0) {
                $scope.tmp.eventMap = {};
                $scope.user.events.map(function(event) {
                    var weekDay = moment(event.start).format("ddd");
                    if (!$scope.tmp.eventMap[weekDay]) {
                        $scope.tmp.eventMap[weekDay] = [];
                    }
                    $scope.tmp.eventMap[weekDay].push(event);
                });
            }
            do {
                //If it is the day of the week
                var scheduleEvents = $scope.tmp.eventMap[s.format("ddd")];

                if (!scheduleEvents) {
                    s = s.add('d', 1);
                    continue;
                }
                var date = s.format("MM-DD-YYYY");
                if (!$scope.tmp.dateRendered[date]) {
                    for (var i = 0; i < scheduleEvents.length; i++) {
                        var scheduleEvent = scheduleEvents[i];
                        var stime = moment(scheduleEvent.start).format("ddd, hh:mma");
                        var etime = moment(scheduleEvent.end).format("ddd, hh:mma");
                        var newEvent = {};
                        for (var k in scheduleEvent) {
                            newEvent[k] = scheduleEvent[k];
                        }
                        newEvent.start = moment(s.format("MM-DD-YYYY Z ") + stime, "MM-DD-YYYY Z ddd, hh:mma").toDate();
                        newEvent.end = moment(s.format("MM-DD-YYYY Z ") + etime, "MM-DD-YYYY Z ddd, hh:mma").toDate();


                        $scope.duplicatedEvents.push(newEvent);
                        $scope.tmp.dateRendered[date] = true;

                    };
                }
                s = s.add('d', 1);

                // console.log(s.format("ddd"));
            } while (s.format("ddd") !== e.format("ddd"));
            //Render the day
        }

        /* config object */
        $scope.uiConfig = {
            calendar: {
                editable: true,
                header: {
                    left: 'agendaWeek,agendaDay',
                    center: '',
                    right: 'prev,next'
                },
                columnFormat: {
                    week: 'ddd',
                },
                defaultView: 'agendaWeek',
                viewRender: function(view, element) {
                    $scope.renderEvent(view.start, view.end);
                },
                selectable: true,
                select: function(start, end, allDay) {
                    //console.log('Day Clicked');
                    //console.log(arguments);
                    $scope.$apply(function() {
                        $scope.currentEvent = $scope.addEvent(start, end, allDay);
                    });
                    $("#myModal").modal('show');
                    //SEGMENT.IO TRACKING CODE
                    analytics.track('Profile-SelectEmptyCalendarSlot', {
                        event: 'ClickOnCalendarEmptySlot'
                    });
                },
                selectHelper: true,
                // dayClick: $scope.alertEventOnClick,
                eventDrop: function(){
                    $scope.save();
                },
                eventResize: function(){
                    $scope.save();
                },
                eventClick: function(event) {
                    $scope.$apply(function() {
                        $scope.currentEvent = event;
                    });
                    $("#myModal").modal('show');
                    //console.log("click event");

                    //SEGMENT.IO TRACKING CODE
                    analytics.track('Profile-SelectCalendarEvent', {
                        event: 'ClickOnExistingCalendar'
                    });
                },
                minTime: 11,
                maxTime: 14,
                // firstHour: 10,
                // height: 250,
                allDaySlot: false,
                slotEventOverlap: false,
                firstDay: (new Date()).getDay()
            }
        };

        $scope.user = Kinvey.getActiveUser();
        if ($scope.user.events === undefined) {
            $scope.events = [];
        }
        else {
            $scope.user.events.forEach(function(event) {
                event.start = moment(event.start).toDate();
                event.end = moment(event.end).toDate();
                updateToActualDate(event);
            });
            $scope.events = $scope.user.events;

        }

        $scope.duplicatedEvents = [];
        /* event sources array*/
        $scope.eventSources = [$scope.events, $scope.duplicatedEvents];

        var jcropapi;
        var initJcrop = function() {
            $timeout(function() {
                var w = $('#target').width();
                var h = $('#target').height();

                $('#target').Jcrop({
                    // start off with jcrop-light class
                    bgOpacity: 0.25,
                    bgColor: 'white',
                    addClass: 'jcrop-dark',
                    aspectRatio: '1',
                    // minSize: [100, 100],
                    // maxSize: [100, 100]
                    onChange: function(c) {
                        $scope.user.pictureCoords = c;
                        //console.log($scope.user.pictureCoords);
                        // var c = $scope.user.pictureCoords;
                        var nw = w * 100 / c.w;
                        var nh = h * 100 / c.h;
                        $scope.user.pictureConvert = "/convert?w=" + nw + "&h=" + nh + "&crop=" + c.x * nw / w + "," + c.y * nh / h + "," + 100 + "," + 100;

                    },
                    onSelect: function(c) {
                        $scope.user.pictureCoords = c;
                        //console.log($scope.user.pictureCoords);

                        var nw = w * 100 / c.w;
                        var nh = h * 100 / c.h;
                        $scope.user.pictureConvert = "/convert?w=" + nw + "&h=" + nh + "&crop=" + c.x * nw / w + "," + c.y * nh / h + "," + 100 + "," + 100;
                    }
                }, function() {
                    jcropapi = this;
                    var c = $scope.user.pictureCoords;
                    if (c === undefined) {
                        jcropapi.setSelect([w / 2 - 50, h / 2 - 50, w / 2 + 100, h / 2 + 100]);
                    }
                    else {
                        jcropapi.setSelect([c.x, c.y, c.x2, c.y2]);
                    }
                    jcropapi.setOptions({
                        bgFade: true
                    });
                    jcropapi.ui.selection.addClass('jcrop-selection');
                });
            });
        };
        // initJcrop();

        //Create the 7 day - row of buttons - starting from today
        $scope.places = ["MIT Campus", "Harvard Campus", "Wellesley Campus"];
        $scope.createEvent = function(event) {
            $("#myModal").modal('hide');
            $scope.save();
        };
        $scope.cancelEvent = function(event) {
            for (var i in $scope.events) {
                var e = $scope.events[i];
                if (e._id == event._id) {
                    $scope.events.splice(i, 1);
                }
            }
            $scope.save();
        };

        // var weekday = new Array(7);
        // weekday[0] = "Sun";
        // weekday[1] = "Mon";
        // weekday[2] = "Tue";
        // weekday[3] = "Wed";
        // weekday[4] = "Thu";
        // weekday[5] = "Fri";
        // weekday[6] = "Sat";
        // $scope.showntimes = [{
        //   from: "11:00am",
        //   to: "11:30am"
        // }, {
        //   from: "11:30am",
        //   to: "12:00pm"
        // }, {
        //   from: "12:00pm",
        //   to: "12:30pm"
        // }, {
        //   from: "12:30pm",
        //   to: "01:00pm"
        // }, {
        //   from: "01:00pm",
        //   to: "01:30pm"
        // }, {
        //   from: "01:30pm",
        //   to: "02:00pm"
        // }, {
        //   from: "02:00pm",
        //   to: "02:30pm"
        // }, {
        //   from: "02:30pm",
        //   to: "03:00pm"
        // }];

        // var firstpart = weekday.slice(0, curIdx);
        // var secondpart = weekday.slice(curIdx, weekday.length);

        // $scope.weekdays = secondpart.concat(firstpart);
        // $scope.weekdays = weekday;

        //$scope.places = [];

        $scope.formatTime = function(event) {
            if (event) {
                return moment(event.start).format("hh:mma") + "-" + moment(event.end).format("hh:mma");
            }
        }

        $scope.remove = function(index) {
            $scope.user.availtime.splice(index, 1);
        }

        $scope.select = function(day, fromTime, toTime) {
            //console.log($scope.user.availtime);
            if ($scope.user.availtime === undefined) {
                $scope.user.availtime = [];
            }
            var result = $scope.user.availtime.filter(function(obj) {
                return obj.day === day && obj.time.from === fromTime && obj.time.to === toTi;
            });
            if (result.length === 0) {
                $scope.user.availtime.push({
                    day: day,
                    time: {
                        from: fromTime,
                        to: toTime
                    }
                });
            }
        }

        // prepare events to upload to kinvey
        var prepareEvents = function(events) {
            return events.map(function(event) {
                return {
                    start: moment(event.start).format(),
                    end: moment(event.end).format(),
                    startUnixTime: moment(event.start).unix(),
                    endUnixTime: moment(event.end).unix(),
                    title: event.title,
                    allDay: event.allDay
                }
            });
        };

        var showError = function(message) {
            $scope.status = "error";
            $scope.message = message;
        }

        $scope.save = function() {
            //console.log($scope.user);

            // Validate
            err = validateUsername($scope.user.username) || validatePhoneNumber($scope.user.phone);
            if (err != null) {
                showError(err);
                return;
            }

            $scope.user.events = prepareEvents($scope.events);
            var user = angular.copy($scope.user);
            $scope.status = "saving";
            $scope.message = "Saving";
            Kinvey.User.update(user, {
                success: function() {
                    //console.log(arguments);
                    $scope.status = "saved";
                    $scope.message = "Save successfully";
                    $timeout(function() {
                        $scope.message = "";
                    }, 2000);
                    // window.location.pathname = "/mainpage.html";
                    $scope.$apply();
                },
                error: function(error) {
                    //console.log(arguments);
                }
            });
        }

        $scope.upload = function() {

            filepicker.pick(function(InkBlob) {
                // jcropapi.destroy();
                $scope.user.picture = InkBlob.url;
                $scope.$apply(function() {
                    $scope.save();
                    // initJcrop();
                });
            });

            // filepicker.pick(function(InkBlob) {
            //     InkBlob.isWriteable = true;
            //     if($scope.user.picture && $scope.user.picture.length > 0){
            //         filepicker.read(InkBlob, function(data) {
            //             filepicker.remove(InkBlob, function(){
            //                 //console.log("Removed");
            //             });
            //             InkBlob.url = $scope.user.picture;
            //             filepicker.write(InkBlob, data,
            //             function(InkBlob) {
            //                 //console.log(JSON.stringify(InkBlob));
            //             }, function(FPError) {
            //                 //console.log("Error: " + FPError.toString());
            //             });
            //         });
            //     }else{
            //         $scope.user.picture = InkBlob.url;
            //     }
            //     //console.log(InkBlob);
            //     $scope.$apply();
            // });

            /*
            filepicker.convert(InkB, {
                    crop: [20, 20, 30, 30]
                },

                
                function(new_InkBlob) {
                    //console.log(new_InkBlob.url);
                    $scope.user.picture = new_InkBlob.url;
                    $scope.$apply();
                });
                */
        }

        // EDITING STUFF --------------------------------------------------------------------------------------------------

        // Variables
        var backup = null;

        // Save a backup of the data and focus on the given element ID and return true
        $scope.editModeOn = function(data, elementIDToFocus) {
            backup = data;
            setTimeout(function() {
                $('#' + elementIDToFocus).focus();
            })
            return true;
        }

        // Attempt to save 
        $scope.editModeOff = function() {
            $scope.save();
            return false;
        };

        // Returns TRUE if 'Enter' or 'Escape' key is pressed
        $scope.onKeyUp = function(e, data) {

            // ESCAPE key : revert to backup
            if (e.keyCode == 27) {
                data = backup;
                return false;
            }

            // ENTER key : turn off edit mode
            if (e.keyCode == 13) {
                return $scope.editModeOff();
                
            }
            return true;
        };


    });
    angular.bootstrap(document, ['app']);

    // var user = Kinvey.getActiveUser();
    // //console.log(user);
    // if(user==null){
    //     window.location.pathname = "./signin.html"
    // }
});
// app = angular.module('app');
// promise.then(function(activeUser) {
//     var promise = Kinvey.User.logout({ 
//         force: true, 
//         success: function() {
//         }
//     });
// }, function(error) {});
