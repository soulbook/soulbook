var promise = Kinvey.init({
    appKey: 'kid_PVehjLya5i',
    appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});

promise.then(function() {

    var promise = Kinvey.User.logout({
        force: true,
    });

    var app = angular.module('app', []);
    window.app = app;
    //console.log("app: ", app);

    app.controller("SignUpController", function($scope) {
        var socket = io.connect();

        $scope.user = {};

        $scope.status = '';

        var checkUniqueUsername = function(username, callback) {

            Kinvey.User.exists(username, {
                success: function(usernameExists) {
                    callback.success(!usernameExists);
                }
            });
        }

        var emailAvailable = function(email, cb) {

            var query = new Kinvey.Query();
            query.equalTo('email', email);

            Kinvey.User.find(query, {
                discover: true,
                success: function(emailExists) {
                    if (emailExists) cb.notavailable();
                    else cb.available();
                },
                error: function(error) {
                    $scope.showError("Error : " + error.name);
                }
            });
        }

        $scope.showError = function(message) {
            $scope.status = 'error';
            $scope.message = message;
            $scope.$apply();
        }

        // If all verification steps passed, call this to create account
        var createAccount = function() {
            $scope.user.picture = "/img/placeholder.png";

            // window.location.pathname = "/mainpage.html"
            $scope.user.events = [];
            Kinvey.User.signup($scope.user, {
                success: function(response) {
                    //console.log(response);
                    $scope.status = 'signedup';
                    $scope.message = 'Your account has been created.  A confirmation email has been sent to ' + $scope.user.email + '.  To activate your account, click on the link included in the email.';
                    $scope.$apply();
                    var promise = Kinvey.User.logout({
                        force: true,
                        success: function() {}
                    });
                },
                error: function(error) {
                    $scope.showError('An error has occurred : ' + error.name);
                    //console.log(arguments);
                }
            });
        }


        // Validate form before requesting to create new account
        $scope.sendSignUpInfo = function() {
            //SEGMENT.IO TRACKING CODE
            analytics.track('SignUpPage-ClickSignUpButton', {
                event: 'Signup'
            });
            // Show attempt status
            $scope.status = 'signingup';
            $scope.message = 'Attempting to sign up ...';


            // Validate username, password, email and phone number
            var err = validateUsername($scope.user.username) || validatePassword($scope.user.password) || validateEmail($scope.user.email) || validatePhoneNumber($scope.user.phone);

            if (err !== null) {
                $scope.showError(err);
                return;
            }

            // Check if username is available
            checkUniqueUsername($scope.user.username, {
                success: function(isUnique) {
                    if (isUnique) {
                        createAccount();
                        // Check if email is available
                        /*
                        emailAvailable($scope.user.email, {
                            available: function()
                            {
                                createAccount();
                            },
                            notavailable: function()
                            {
                                $scope.showError('This email address has already been taken.  Please use another address.');
                            }
                        });
                        */
                    }
                    else {
                        $scope.showError('Username already exists.');
                    }
                },
                error: function() {
                    $scope.showError(("An unexpected error has occurred."));
                }
            });
        }

        $scope.redirectSignin = function() {
            window.location.pathname = "/signin.html"
        }
    });

    angular.bootstrap(document, ['app']);

    // var user = Kinvey.getActiveUser();
    // //console.log(user);
    // if(user==null){
    //     window.location.pathname = "./signin.html"
    // }
});
