var promise = Kinvey.init({
    appKey: 'kid_PVehjLya5i',
    appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});

window.socket = io.connect();

promise.then(function() {
    var user = _.clone(Kinvey.getActiveUser());
    if (user === null) {
        window.location.pathname = "/signin.html";
    }

    function addDate(date, day) {
        return new Date(date.getTime() + 86400000);
    }

    var app = angular.module('app', ['ui.calendar', 'ui.bootstrap']);
    window.app = app;

    var updateToActualDate = function(event) {
        var now = moment();
        var day = now.day();
        var start = moment(event.start);
        start.year(now.year());
        start.month(now.month());
        start.date(now.date());
        if (start.day() > moment(event.start).day()) {
            start.day(moment(event.start).day());
            start = start.add('w', 1);
        }
        else {
            start.day(moment(event.start).day());
        }
        // start.date(now.date()+(start.day()-day));
        // console.log("start.day()",start.day());
        event.start = start.toDate();
        var end = moment(event.end);
        end.year(now.year());
        end.month(now.month());
        end.date(now.date());
        if (end.day() > moment(event.end).day()) {
            end.day(moment(event.end).day());
            end = end.add('w', 1);
        }
        else {
            end.day(moment(event.end).day());
        }
        // end.date(now.date()+(end.day()-day));
        end.day(moment(event.end).day());
        event.end = end.toDate();

    }

    // function calculateCalendarDate(user) {
    //   //Get the actual date
    //   var now = moment();
    //   var availtime = user.availtime;
    //   if (availtime === undefined) {
    //     return;
    //   }

    //   for (var i = 0; i < 7; i++) {
    //     var day = now.add('days', 1);
    //     var weekday = day.format("ddd");
    //     console.log("weekday: ", weekday);

    //     for (var j in availtime) {
    //       var time = availtime[j];
    //       if (time.day === weekday) {
    //         time.date = day.format('MMM, DD');
    //         time.unix = day.unix();
    //       }
    //     }
    //   }

    //   user.availtime = availtime.sort(function(a, b) {
    //     return a.unix - b.unix;
    //   });
    // }

    app.controller("MainPageController", function($scope, $timeout) {

        // INITIALIZE ----------------------------------------------------------------------

        $scope.infoTime = "Click the time slot to filter by time.";
        $scope.infoEdit = "Click edit to edit the time slot."
        //pass the data of the active users into scope variable user
        $scope.user = user;


        // SOCKET STUFF ----------------------------------------------------------------------

        socket.emit('subscribe', {
            room: user.username
        });

        //SEGMENT.IO TRACKING CODE
        analytics.identify($scope.user._id, {
            email: $scope.user.email,
        });


        // INVITATION STUFF ------------------------------------------------------------------------

        // Constants & Variables
        var locationSuggestions = ["Student Center", "Barker Library", "Hayden Library", "Dewey Library", "Martin Trust Center", "77 Massachusetts Avenue (Mass Ave)", "Z-center (Z center)", "Kregs Auditorium", "The Coop", "Media Lab", "Stata Cafe", "Brain and Cognitive Sciences Atrium", "Koch Institute", "Ashdown House", "Sidney-Pacific House", "Edgerton House", "Tang Hall", "Maseeh Hall", "McCormick Hall", "Basker House", "Senior House", "Burton-Conner House", "MacGregor House", "New House", "Next House", "La Verde's Market ", "Anna's Taqueria", "Lobdell Food Court"];

        // Send the invitation to another user
        $scope.sendInvitation = function(invitedUser, availtime, event) {
            //SEGMENT.IO TRACKING CODE
            analytics.track('Mainpage-User-SendInvitation', {
                event: 'Click'
            });

            $scope.selected = undefined;
            $scope.locationSuggestions = locationSuggestions;
            $scope.invitedUser = invitedUser;

            // Setup invitation form
            $scope.invitedUser.warnmessage = "";
            $scope.showEditLocation = false;
            $scope.showEditMessage = false;

            // $('#locationInput').typeahead({local: locationSuggestions});

            if (moment(event.start).unix() < moment().unix()) {
                $scope.invitedUser.warnmessage += "The time has already passed. "
                //SEGMENT.IO TRACKING CODE
                analytics.track('Mainpage-User-SelectPastTime', {
                    event: 'ClickCalendar'
                });
            }
            var query = new Kinvey.Query();
            query.notEqualTo('status', "accept").and().notEqualTo('status', "decline");
            query.greaterThanOrEqualTo('startUnixTime', moment().unix());
            query.equalTo("time", availtime.time);
            query.equalTo("day", availtime.day);
            query.equalTo("date", availtime.date);
            query.equalTo("place", availtime.place);
            query.equalTo("from._id", $scope.user._id);
            query.equalTo("to._id", $scope.invitedUser._id);
            // query.ascending('startUnixTime');
            Kinvey.DataStore.count("meetings", query, {
                success: function(count) {
                    if (count > 0) {
                        $scope.invitedUser.warnmessage += "You have already sent this invite. "
                        $scope.$apply();
                    }
                },
                error: function() {

                }
            });


            query = new Kinvey.Query();
            query.greaterThanOrEqualTo('startUnixTime', moment(event.start).unix()).and().lessThanOrEqualTo('startUnixTime', moment(event.end).unix());
            query.equalTo("from._id", $scope.user._id);
            query.equalTo('status', "accept");
            query.ascending('startUnixTime');
            Kinvey.DataStore.count("meetings", query, {
                success: function(count) {
                    if (count > 0) {
                        $scope.invitedUser.warnmessage += "You have already scheduled a meeting at this time. "
                        $scope.$apply();
                    }
                },
                error: function() {}
            });

            $scope.invitedUser.event = {
                start: moment(event.start).format(),
                end: moment(event.end).format(),
                title: event.title,
                place: event.title,
                allDay: event.allDay
            };
            $scope.invitedUser.place = availtime.place;
            $scope.invitedUser.date = availtime.date;
            $scope.invitedUser.day = availtime.day;
            $scope.invitedUser.time = availtime.time;
            $scope.invitedUser.message = "I want to meet with you."
            $("#myModal").one('shown.bs.modal', function() {
                $scope.$apply();
            });
            $('#myModal').modal({});

        }

        // allow message to be editable
        $scope.editMessage = function() {
            if ($scope.showEditMessage == false) {
                $scope.showEditMessage = true;
                setTimeout(function() {
                    $("#msgInput").focus();
                }, 0);
            }
        }
        
        

        // allow user to edit  location    
        $scope.editLocation = function() {
            //SEGMENT.IO TRACKING CODE
            analytics.track('Mainpage-User-Calendar-EditLocation', {
                event: 'Click'
            });
            if ($scope.showEditLocation == false) {
                $scope.showEditLocation = true;
                setTimeout(function() {
                    $("#locationInput").focus();
                }, 0);
            }

        }

        // send meeting data to kinvey datastore : in "meetings" database
        $scope.sendMessage = function() {
            //SEGMENT.IO TRACKING CODE
            analytics.track('Mainpage-User-SendInvitation', {
                event: 'Click',
                user_id: $scope.user._id,
                other_user_id: $scope.invitedUser._id
            });

            if (!$scope.invitedUser.detailedPlace) {
                $scope.invitedUser.dangermessage = "Please specify a detailed location you want to meet.";
                return;
            }

            var meeting = {};
            meeting.from = {
                '_id': $scope.user._id
            };
            meeting.to = {
                '_id': $scope.invitedUser._id
            };
            meeting.place = $scope.invitedUser.place;
            meeting.date = $scope.invitedUser.date;
            meeting.day = $scope.invitedUser.day;
            meeting.time = $scope.invitedUser.time;
            meeting.detailedPlace = $scope.invitedUser.detailedPlace;
            meeting.status = "sent";
            meeting.event = $scope.invitedUser.event;
            meeting.startUnixTime = moment($scope.invitedUser.event.start).unix();
            meeting.endUnixTime = moment($scope.invitedUser.event.end).unix();

            meeting.message = $scope.invitedUser.message;

            var acl = new Kinvey.Acl(meeting);
            // Add read permissions to user “John Doe” with _id “johndoe”.
            acl.addReader(meeting.from._id);
            acl.addWriter(meeting.from._id);
            acl.addReader(meeting.to._id);
            acl.addWriter(meeting.to._id);

            Kinvey.DataStore.save("meetings", angular.copy(meeting), {
                success: function(response) {
                    $('#myModal').modal('hide');
                    socket.emit("message", {
                        room: meeting.to.username,
                        event: "invite",
                        meeting: response
                    });
                },
                error: function(error) {
                    $('#myModal').modal('hide');
                    alert('failed');
                    alert(error.name);
                }
            });
        }

        // CALENDAR STUFF --------------------------------------------------------

        $scope.renderEvent = function(user, start, end) {
            var s = moment(start);
            var e = moment(end);
            // Fill in the day that has no events yet
            // TODO: update the day that has some events but in correct time
            if (!user.dateRendered) {
                user.dateRendered = {};
                if (user.events) {
                    user.events.map(function(event) {
                        user.dateRendered[moment(event.start).format("MM-DD-YYYY")] = true;
                    });
                }
            }
            if (!user.eventMap || user.eventMap.length === 0) {
                user.eventMap = {};
                if (user.events) {
                    user.events.map(function(event) {
                        var weekDay = moment(event.start).format("ddd");
                        if (!user.eventMap[weekDay]) {
                            user.eventMap[weekDay] = [];
                        }
                        user.eventMap[weekDay].push(event);
                    });
                }
            }
            do {
                //If it is the day of the week
                var scheduleEvents = user.eventMap[s.format("ddd")];

                if (!scheduleEvents) {
                    s = s.add('d', 1);
                    continue;
                }
                var date = s.format("MM-DD-YYYY");
                if (!user.dateRendered[date]) {
                    for (var i = 0; i < scheduleEvents.length; i++) {
                        var scheduleEvent = scheduleEvents[i];
                        var stime = moment(scheduleEvent.start).format("ddd, hh:mma");
                        var etime = moment(scheduleEvent.end).format("ddd, hh:mma");
                        var newEvent = {};
                        for (var k in scheduleEvent) {
                            newEvent[k] = scheduleEvent[k];
                        }
                        newEvent.start = moment(s.format("MM-DD-YYYY Z ") + stime, "MM-DD-YYYY Z ddd, hh:mma").toDate();
                        newEvent.end = moment(s.format("MM-DD-YYYY Z ") + etime, "MM-DD-YYYY Z ddd, hh:mma").toDate();


                        user.duplicateevents.push(newEvent);
                        user.dateRendered[date] = true;

                    };
                }
                s = s.add('d', 1);

                // console.log(s.format("ddd"));
            } while (s.format("ddd") !== e.format("ddd"));
            //Render the day

        }

        /* config object */
        $scope.uiConfig = {
            calendar: {
                editable: true,
                header: {
                    left: 'agendaWeek,agendaDay',
                    center: '',
                    right: 'prev,next'
                },
                columnFormat: {
                    week: 'ddd MM.dd',
                },
                defaultView: 'agendaWeek',
                viewRender: function(view, element) {
                    $scope.renderEvent(user, view.start, view.end);

                },
                dayClick: function(event) {

                },
                eventDrop: function(event) {

                },
                eventResize: function(event) {

                },
                eventClick: function(event) {
                    $scope.infoTime = "";
                    //SEGMENT.IO TRACKING CODE
                    analytics.track('Mainpage-User-SelectCalendarEvent', {
                        event: 'Click',
                        user_id: user._id
                    });

                    // console.log(event);
                    if (event.className.length > 0) {
                        event.className = [];
                        delete $scope.curFilters[event._id];

                    }
                    else {
                        event.className = ["red"];
                        $scope.curFilters[event._id] = {
                            startUnixTime: event.startUnixTime,
                            endUnixTime: event.endUnixTime
                        };
                    }
                    // console.log("$scope.curFilters");
                    $scope.queryUser($scope.query, 1);





                    //   $scope.user.events.push(event);
                    //   $("div[ui-calendar='uiConfig.calendar']").fullCalendar( 'refresh' );
                    // var startTime = moment(event.start);
                    // var endTime = moment(event.end);
                    // $('#infoModal').modal({});
                    // $scope.currentInfo = {
                    //   day: startTime.format("ddd"),
                    //   date: startTime.format("MMM, DD"),
                    //   time: {
                    //     from: startTime.format("hh:mma"),
                    //     to: endTime.format("hh:mma")
                    //   },
                    //   place: event.title || event.place
                    // };
                    $scope.$apply();
                },
                minTime: 11,
                maxTime: 14,
                // firstHour: 10,
                // height: 250,
                allDaySlot: false,
                slotEventOverlap: false,
                disableDragging: true,
                disableResizing: true,
                firstDay: (new Date()).getDay()
            }
        };

        if ($scope.user.events === undefined) {
            $scope.user.events = [];
        }
        else {

            $scope.user.events.forEach(function(event) {
                updateToActualDate(event);
            });
        }

        $scope.user.confimedevents = [];

        var query = new Kinvey.Query();
        query.equalTo('from._id', $scope.user._id).or().equalTo('to._id', $scope.user._id);
        query.equalTo('status', "accept");
        query.greaterThanOrEqualTo('startUnixTime', moment().unix());
        query.ascending('startUnixTime');
        var promise = Kinvey.DataStore.find("meetings", query, {
            success: function(response) {
                var confirmedmeetings = response;
                // updateUserInfo($scope.confirmedmeetings);

                // update calendar events
                confirmedmeetings.forEach(function(meeting) {
                    meeting.event.color = "purple";
                    meeting.event.start = moment(meeting.event.start).toDate();
                    meeting.event.end = moment(meeting.event.end).toDate();
                    $scope.user.confimedevents.push(meeting.event);
                });

                $scope.$apply();
            }
        });
        $scope.user.duplicateevents = [];

        /* event sources array*/
        $scope.user.eventSources = [$scope.user.events, $scope.user.duplicateevents]; //[$scope.user.events, $scope.user.confimedevents, $scope.user.duplicateevents];
        console.log($scope.user.eventSources);

        $scope.findOtherUserById = function(id) {
            if ($scope.other_users) {
                for (var i = 0; i < $scope.other_users.length; i++) {
                    var tmpuser = $scope.other_users[i];
                    if (tmpuser._id === id) {
                        return tmpuser;
                    }
                };
            }
        }

        $scope.addEvent = function(start, end, allDay) {

            var m = 1;
            var y = 2009;

            var startMoment = moment(new Date(y, m));
            startMoment.day(start.getDay());
            startMoment.hour(start.getHours());
            startMoment.minute(start.getMinutes());

            var endMoment = moment(new Date(y, m));
            endMoment.day(end.getDay());
            endMoment.hour(end.getHours());
            endMoment.minute(end.getMinutes());

            //console.log("d", d);
            //console.log(h);
            var start = startMoment.toDate();
            var end = endMoment.toDate();
            var event = {
                title: 'MIT Campus',
                start: start,
                end: end,
                startUnixTime: Math.floor(start.getTime() / 1000),
                endUnixTime: Math.floor(end.getTime() / 1000),
                allDay: allDay
            };
            console.log("start.getDay()", start.getDay());
            console.log("end.getDay()", end.getDay());

            updateToActualDate(event);
            $scope.user.events.push(event);
            return event;
        };

        $scope.places = ["MIT Campus", "Harvard Campus", "Wellesley Campus"];
        $scope.createEvent = function(event) {
            $("#editModal").modal('hide');
            $scope.save();
        };
        $scope.cancelEvent = function(event) {
            for (var i in $scope.user.events) {
                var e = $scope.user.events[i];
                if (e._id == event._id) {
                    $scope.user.events.splice(i, 1);
                }
            }
            $scope.save();
        };

        $scope.uiEditConfig = {
            calendar: {
                editable: true,
                header: {
                    left: 'agendaWeek,agendaDay',
                    center: '',
                    right: 'prev,next'
                },
                columnFormat: {
                    week: 'ddd',
                },
                defaultView: 'agendaWeek',
                viewRender: function(view, element) {
                    $scope.renderEvent($scope.user, view.start, view.end);
                },
                selectable: true,
                select: function(start, end, allDay) {
                    //console.log('Day Clicked');
                    //console.log(arguments);
                    $scope.$apply(function() {
                        $scope.currentEvent = $scope.addEvent(start, end, allDay);
                    });
                    $("#editModal").modal('show');
                },
                selectHelper: true,
                // dayClick: $scope.alertEventOnClick,
                eventDrop: function() {
                    $scope.save();
                },
                eventResize: function() {
                    $scope.save();
                },
                eventClick: function(event) {
                    $scope.$apply(function() {
                        $scope.currentEvent = event;
                    });
                    $("#editModal").modal('show');
                    //console.log("click event");
                },
                minTime: 11,
                maxTime: 14,
                // firstHour: 10,
                // height: 250,
                allDaySlot: false,
                slotEventOverlap: false,
                firstDay: (new Date()).getDay()
            }
        };

        $scope.editCalendar = function() {
            $scope.infoEdit = "";
            if ($scope.editButtonStatus === "Done") {
                $scope.editButtonStatus = "Edit";
            }
            else {
                $scope.editButtonStatus = "Done";
            }
        }
        $scope.invite = function(invitedUser){

          var availtime = {
              place: "MIT Campus",
              date: moment().format("MMM, DD"),
              day: moment().format("ddd"),
              time: {
                  from: moment().format("hh:mma"),
                  to: moment().add('m',60).format("hh:mma")
              },
          };

          // $scope.$apply(function() {
              $scope.sendInvitation(invitedUser, availtime, event);
          // });
        }
        //Other people calendar
        $scope.otherUiConfig = {
            calendar: {
                editable: true,
                header: {
                    left: 'agendaWeek,agendaDay',
                    center: '',
                    right: 'prev,next'
                },
                columnFormat: {
                    week: 'ddd MM.dd',
                },
                defaultView: 'agendaWeek',
                viewRender: function(view, element) {
                    var id = element.parentsUntil("div[ui-calendar]").parent().parent().find("div[data='id']").html();
                    var tmpuser = $scope.findOtherUserById(id);
                    if (tmpuser) {
                        $scope.renderEvent(tmpuser, view.start, view.end);
                    }
                },
                eventClick: function(event) {


                    var availtime = {
                        place: event.title,
                        date: moment(event.start).format("MMM, DD"),
                        day: moment(event.start).format("ddd"),
                        time: {
                            from: moment(event.start).format("hh:mma"),
                            to: moment(event.end).format("hh:mma")
                        },
                    };

                    $scope.$apply(function() {

                        $scope.sendInvitation(event.invitedUser, availtime, event);
                    });


                },
                minTime: 11,
                maxTime: 14,
                // firstHour: 10,
                // height: 250,
                allDaySlot: false,
                slotEventOverlap: false,
                firstDay: (new Date()).getDay(),
                disableDragging: true,
                disableResizing: true
            }
        };

        // MEMBER PAGES STUFF -------------------------------------------------------------------

        // <<<<<<< HEAD
        // }
        // Calendar stuff

        /* config object */

        // =======
        // Constants
        // >>>>>>> 009cdf365afc266e3f9b7febbdf7e2c8519525d1

        var LIMIT_PER_PAGE = 5;

        // Variables

        $scope.total = 0;
        $scope.pageNumber = 1;
        $scope.totalPages = 1;

        // Functions 


        // <<<<<<< HEAD
        //Other people calendar

        $scope.nextPage = function() {
            //SEGMENT.IO TRACKING CODE
            analytics.track('Finder-SelectNextpage', {
                event: 'Click'
            });
            $scope.pageNumber++;
            if ($scope.pageNumber > $scope.totalPages) {
                $scope.pageNumber = $scope.totalPages;
            }
            $scope.queryUser($scope.query, $scope.pageNumber);
        }
        $scope.goto = function(i) {
            $scope.pageNumber = i;
            $scope.queryUser($scope.query, $scope.pageNumber);
        }
        $scope.prevPage = function() {
            $scope.pageNumber--;
            if ($scope.pageNumber < 1) {
                $scope.pageNumber = 1;

            }
            $scope.queryUser($scope.query, $scope.pageNumber);
        }
        $scope.total = 0;
        $scope.pageNumber = 1;
        $scope.totalPages = 1;

        var processOtherUser = function(other_user) {

            if (other_user.events === undefined) {
                other_user.events = [];
            }
            else {
                other_user.events.forEach(function(event) {
                    updateToActualDate(event);
                    event.invitedUser = {
                        username: other_user.username,
                        _id: other_user._id,
                        picture: other_user.picture
                    };
                });
            }
            // $("div[ui-calendar='otherUiConfig.calendar']").fullCalendar('refetchEvents');
            other_user.events.forEach(function(event1) {
                $scope.user.events.forEach(function(event2) {
                    var edgeS = moment(event1.start).unix() >= moment(event2.start).unix();
                    var edgeE = moment(event1.end).unix() <= moment(event2.end).unix();

                    if (edgeS && edgeE) {
                        event1.color = "green";
                    }
                });
            });
            other_user.duplicateevents = [];
            other_user.eventSources = [other_user.events, other_user.duplicateevents];
            console.log(other_user.eventSources);

            // Return the times that are in common with the current user
            // for (var i in $scope.other_users) {
            //   var other_user = $scope.other_users[i];
            //   calculateCalendarDate(other_user);
            //   //Loop through availble time of the user
            //   for (var j in other_user.availtime) {
            //     var availtime = other_user.availtime[j];
            //     //alert("User " + user.username + " : " + availtime.day + availtime.time);
            //     //Compare with current user's available time
            //     for (var z in $scope.user.availtime) {
            //       var curUserAvail = $scope.user.availtime[z];
            //       //Checking statement here
            //       if ((curUserAvail.day == availtime.day) && (curUserAvail.time == availtime.time)) {
            //         //alert("Other User " + user.username + " : " + availtime.day + availtime.time + " \nUser " + $scope.user.username + " : " + curUserAvail.day + curUserAvail.time);
            //         // Set commontime variable to true to change line color
            //         $scope.other_users[i].availtime[j].common = true;
            //         //alert($scope.other_users[i].availtime[j].common);
            //       }
            //     }
            //   }
            // }
        }
        $scope.curFilters = [];

        var buildUserQuery = function(queryText) {
            var query = new Kinvey.Query();
            var regex = new RegExp(".*" + queryText + ".*", "i");
            var all = [];
            for (var i in $scope.curFilters) {
                var curFilter = $scope.curFilters[i];
                // query.greaterThanOrEqualTo('events.startUnixTime', curFilter.startUnixTime).and().lessThanOrEqualTo('events.startUnixTime', curFilter.endUnixTime);
                // query.greaterThanOrEqualTo('events.endUnixTime', curFilter.startUnixTime).and().lessThanOrEqualTo('events.endUnixTime', curFilter.endUnixTime);
                // query.equalTo("events.startUnixTime", curFilter.startUnixTime);
                // query.equalTo("events.endUnixTime", curFilter.endUnixTime);
                all.push({
                    "$elemMatch": {
                        startUnixTime: {
                            "$gte": curFilter.startUnixTime,
                            "$lte": curFilter.endUnixTime
                        },
                        endUnixTime: {
                            "$gte": curFilter.startUnixTime,
                            "$lte": curFilter.endUnixTime
                        }
                    }
                });
                // query.or();
            }
            if (all.length > 0) {
                query.equalTo("events", {
                    "$all": all
                });
            }
            // query.equalTo("username", {"$regex":".*.*","$options":"i"});

            // query.matches('username', regex).or().matches('status', regex).or().matches('email', regex).or().matches('firstName', regex).or().matches('lastName', regex);
            if (queryText) {
                query.matches('status', regex);
            }
            query.notEqualTo('_id', $scope.user._id);
            query.notEqualTo('username', "email");
            return query;
        }
        $scope.queryUser = function(queryText, page) {
            //SEGMENT.IO TRACKING CODE
            analytics.track('Mainpage-Search-UserSearch', {
                event: 'Click',
                searchPhrase: queryText
            });

            console.log("query");
            if (page === undefined) {
                page = 1;
            }

            if (queryText === undefined) {
                queryText = "";
            }

            // Set up query            
            var query = buildUserQuery(queryText);
            console.log("filter", $scope.curFilters);

            // Count the number of members
            Kinvey.User.count(query, {
                success: function(response) {
                    $scope.total = response;
                    $scope.totalPages = Math.ceil($scope.total / LIMIT_PER_PAGE);
                    $scope.pageNumberArray = [];
                    for (var i = 1; i <= $scope.totalPages; i++) {
                        $scope.pageNumberArray.push(i);
                    };
                    $scope.$apply();
                },
            });

            // Setup query to fetch a limited number of members
            query.limit(LIMIT_PER_PAGE);
            query.skip(LIMIT_PER_PAGE * (page - 1)); // The first page, shows results 020.

            // Retrieve result
            Kinvey.User.find(query, {
                success: function(response) {
                    $scope.other_users = response;
                    $scope.other_users.forEach(function(other_user) {
                        // other_user.eventSources = [];
                        processOtherUser(other_user);
                    });
                    $scope.findBestMatchNew(queryText);
                    $scope.$apply();
                }
            });
        }

        // MATCHMAKING STUFF ---------------------------------------------------------------

        // Ban list - ignore common words
        var ban_list = ['the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'i', 'it', 'for', 'not', 'on', 'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 'or', 'an', 'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which', 'go', 'me', 'when', 'make', 'can', 'like', 'time', 'no', 'just', 'him', 'know', 'take', 'people', 'into', 'year', 'your', 'good', 'some', 'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also', 'back', 'after', 'use', 'two', 'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new', 'want', 'because', 'any', 'these', 'give', 'day', 'most', 'us'];
        var NUMBER_OF_SUGGESTIONS = 1;
        $scope.suggestion = null;

        // score function: returns a score that indicates how much the 2 tokens match
        var score_fx = function(token1, token2) {

            // Null check
            if (token1 == null || token2 == null) return 0;

            // ignore common words
            if (ban_list.indexOf(token1) != -1 || ban_list.indexOf(token2) != -1) return 0;

            // get the shorter word
            var shorter = token1;
            var longer = token2;
            if (token1.length > token2.length) {
                shorter = token2;
                longer = token1;
            }

            // their lengths cannot differ by more than 2
            if (shorter.length + 2 < longer.length) return 0;

            // check if shorter is a substring of longer
            if (longer.indexOf(shorter) != 0) return 0;

            // give higher score based on length of word
            return shorter.length * shorter.length;

        }

        // returns a score indicating the degree to which the two statuses match
        var matchScore = function(statusA, statusB) {
            if (statusA == null || statusB == null || statusA == "" || statusB == "") return 0;

            var score = 0;

            //Tokenize and sort
            var A_Tokens = statusA.toLowerCase().match(/\w+/g).sort();
            var B_Tokens = statusB.toLowerCase().match(/\w+/g).sort();

            //Find common token and increase score
            var A_pointer = 0;
            var B_pointer = 0;
            while (A_pointer < A_Tokens.length && B_pointer < B_Tokens.length) {

                // Increase score if matched
                score += score_fx(A_Tokens[A_pointer], B_Tokens[B_pointer]);

                // Advance pointer
                if (A_Tokens[A_pointer] < B_Tokens[B_pointer]) A_pointer++;
                else B_pointer++;
            }
            return score;

        }

        $scope.findBestMatchNew = function(queryText) {
            var query = new Kinvey.Query();
            var regex = new RegExp(".*" + queryText + ".*", "i");
            var all = [];
            for (var i in $scope.curFilters) {
                var curFilter = $scope.curFilters[i];
                // query.greaterThanOrEqualTo('events.startUnixTime', curFilter.startUnixTime).and().lessThanOrEqualTo('events.startUnixTime', curFilter.endUnixTime);
                // query.greaterThanOrEqualTo('events.endUnixTime', curFilter.startUnixTime).and().lessThanOrEqualTo('events.endUnixTime', curFilter.endUnixTime);
                // query.equalTo("events.startUnixTime", curFilter.startUnixTime);
                // query.equalTo("events.endUnixTime", curFilter.endUnixTime);
                all.push({
                    "$elemMatch": {
                        startUnixTime: {
                            "$gte": curFilter.startUnixTime,
                            "$lte": curFilter.endUnixTime
                        },
                        endUnixTime: {
                            "$gte": curFilter.startUnixTime,
                            "$lte": curFilter.endUnixTime
                        }
                    }
                });
            }
            if (all.length > 0) {
                query.equalTo("matchEvents", {
                    "$all": all
                });
            }
            if (queryText) {
                query.matches('matchStatus', regex);
            }
            query.equalTo('username', $scope.user.username);
            query.descending('score');
            query.limit(NUMBER_OF_SUGGESTIONS);

            Kinvey.DataStore.find("matches", query, {
                success: function(matches) {
                    var suggestions = [];
                    async.each(matches, function(match, cb) {
                        var query = new Kinvey.Query();
                        query.equalTo("username", match.match);
                        Kinvey.User.find(query, {
                            success: function(response) {
                                if (response.length > 0) {
                                    suggestions.push(response[0]);
                                }
                                cb();
                            },
                            error: function(err) {
                                cb();
                            }
                        });
                    }, function() {
                        console.log("suggestions", suggestions);
                        suggestions.forEach(function(suggestion) {
                            var matched = false;
                            // Add to the front if exists. Otherwise just badge it and move to the front
                            for (var i = 0; i < $scope.other_users.length; i++) {
                                var otherUser = $scope.other_users[i];
                                otherUser.suggested = false;
                                console.log("otherUser", otherUser);
                                if (otherUser._id === suggestion._id) {
                                    otherUser.suggested = true;
                                    var removedUsers = $scope.other_users.splice(i, 1);
                                    $scope.other_users.splice(0, 0, removedUsers[0]);
                                    matched = true;
                                    break;
                                }
                            }
                            if (!matched) {
                                suggestion.suggested = true;
                                processOtherUser(suggestion);
                                console.log("suggestion", suggestion);
                                console.log("$scope.other_users", $scope.other_users);
                                $scope.other_users.splice(0, 0, suggestion);

                            }
                        });
                        $scope.$apply();
                    });

                },
                error: function(err) {

                }
            });

        }
        // return best match using user's profile and other users' profiles
        $scope.findBestMatch = function(queryText) {
            var query = buildUserQuery(queryText);

            // var maxScore = 0;
            // var suggestions = [];
            // $scope.other_users.forEach(function(another_user) {

            //             var score = matchScore($scope.user.status, another_user.status);
            //             if (score >= maxScore) {
            //                 maxScore = score;
            //                 if (suggestions.length == NUMBER_OF_SUGGESTIONS) suggestions.shift();
            //                 suggestions.push(another_user);
            //             }

            //         });
            // suggestions.reverse();
            // for (var i = 0; i < Math.min(suggestions.length,3); i++) {

            // }
            // return;

            // Set up query            
            // var query = new Kinvey.Query();

            // query.notEqualTo('_id', $scope.user._id);
            // query.notEqualTo('username', "email");

            // Retrieve result
            Kinvey.User.find(query, {
                success: function(response) {

                    var maxScore = 0;
                    var suggestions = [];

                    response.forEach(function(another_user) {

                        var score = matchScore($scope.user.status, another_user.status);
                        if (score >= maxScore) {
                            maxScore = score;
                            if (suggestions.length == NUMBER_OF_SUGGESTIONS) suggestions.shift();
                            suggestions.push(another_user);
                        }

                    });

                    // reverse order so best match is in front
                    suggestions.reverse();

                    suggestions.forEach(function(suggestion) {
                        var matched = false;
                        // Add to the front if exists. Otherwise just badge it and move to the front
                        for (var i = 0; i < $scope.other_users.length; i++) {
                            var otherUser = $scope.other_users[i];
                            if (otherUser._id === suggestion._id) {
                                otherUser.suggested = true;
                                var removedUsers = $scope.other_users.splice(i, 1);
                                $scope.other_users.splice(0, 0, removedUsers[0]);
                                matched = true;
                                break;
                            }
                        }
                        if (!matched) {
                            suggestion.suggested = true;
                            processOtherUser(suggestion);
                            $scope.other_users.splice(0, 0, suggestion);
                        }
                        // Old stuff
                        // processOtherUser(suggestion);
                    });

                    $scope.suggestions = suggestions;
                    $scope.$apply();
                }
            });
        }

        $scope.toggleSuggestion = function(suggestion) {

            // toggle on
            if ($scope.suggestion == null) {
                $("#collapsibleSuggestionBox").collapse('toggle');
                $scope.suggestion = suggestion
            }
            // toggle off
            else if ($scope.suggestion == suggestion) {
                $("#collapsibleSuggestionBox").collapse('toggle');
                $scope.suggestion = null;
            }
            // update suggestion
            else {
                $scope.suggestion = suggestion;
            }
            // $scope.$apply();
            // $scope.$apply();
        }

        // EDITING STUFF --------------------------------------------------------------------------------------------------

        // Variables
        var backup = null;

        // Save a backup of the data and focus on the given element ID and return true
        $scope.editModeOn = function(data, elementIDToFocus) {
            backup = data;
            setTimeout(function() {
                $('#' + elementIDToFocus).focus();
            })
            return true;
        }

        // Attempt to save 
        $scope.editModeOff = function() {
            $scope.save();
            return false;
        };

        // Returns TRUE if 'Enter' or 'Escape' key is pressed
        $scope.onKeyUp = function(e, data) {

            // ESCAPE key : revert to backup
            if (e.keyCode == 27) {
                data = backup;
                return false;
            }

            // ENTER key : turn off edit mode
            if (e.keyCode == 13) {
                return $scope.editModeOff();
            }
            return true;
        };

        // SAVE STUFF ------
        // prepare events to upload to kinvey
        var prepareEvents = function(events) {
            return events.map(function(event) {
                return {
                    start: moment(event.start).format(),
                    end: moment(event.end).format(),
                    startUnixTime: moment(event.start).unix(),
                    endUnixTime: moment(event.end).unix(),
                    title: event.title,
                    allDay: event.allDay
                }
            });
        };

        $scope.save = function() {
            var activeUser = Kinvey.getActiveUser();
            console.log(activeUser);
            var keys = Object.keys(activeUser);
            
            for (var i = keys.length - 1; i >= 0; i--) {
                var key = keys[i];
                activeUser[key] = $scope.user[key];
            };
            activeUser.status = $scope.user.status;
            activeUser.events = prepareEvents(activeUser.events);
            
            $scope.status = "saving";
            $scope.notificationMessage = "Saving";
            
            Kinvey.User.update(angular.copy(activeUser), {
                success: function() {
                    //console.log(arguments);
                    $scope.status = "saved";
                    $scope.notificationMessage = "Save successfully";
                    $timeout(function() {
                        $scope.notificationMessage = "";
                    }, 2000);
                    // window.location.pathname = "/mainpage.html";
                    $scope.$apply();
                },
                error: function(error) {
                    //console.log(arguments);
                }
            });
        }

        // ACTION -----------------------------------------------------------------------------

        // Automatically load users for page 1
        $scope.queryUser("", 1);
        // $scope.findBestMatch();
    });
    angular.bootstrap(document, ['app']);
});