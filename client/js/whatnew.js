var promise = Kinvey.init({
    appKey: 'kid_PVehjLya5i',
    appSecret: '0fc4b05dd61149bb86c572e9d027410a'
});

window.socket = io.connect();

promise.then(function() {
    var user = _.clone(Kinvey.getActiveUser());
    if (user === null) {
        window.location.pathname = "/signin.html";
    }

    var app = angular.module('app', ['ui.calendar', 'ui.bootstrap']);
    window.app = app;

    app.controller("WhatNewController", function($scope, $timeout) {
      $scope.user = user;
      $scope.promos = [
      {
        picture: "./assets/images/mocks/data1.jpg",      
        title: "",
        description: ""
      }, 
      {
        picture: "./assets/images/mocks/data2.jpg",      
        title: "",
        description: ""
      },
      {
        picture: "./assets/images/mocks/data3.jpg",       
        title: "",
        description: ""
      },
      {
        picture: "./assets/images/mocks/data4.jpg",       
        title: "",
        description: ""
      },
      {
        picture: "./assets/images/mocks/data5.jpg",       
        title: "",
        description: ""
      }
      ];
    });
    
    angular.bootstrap(document, ['app']);
});