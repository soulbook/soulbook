
var conversionTable = {
        "can't":"cannot",
        "won't":"will not"
};

var rules = [
        { regex: /([az]*)\'s/g, output: "$1 is" },
        { regex: /([az]*)\'ll/g, output: "$1 will" },
        { regex: /([az]*)\'t/g, output: "$1 not" },
        { regex: /([az]*)\'re/g, output: "$1 are" },
];

// Accepts a list of tokens to expand.
var normalize_tokens = function(tokens) {
        var rule_count = rules.length;
        var num_tokens = tokens.length;
        for(var i = 0 ; i < num_tokens ; i++) {
                var token = tokens[i];

                // check the covnersion table.
                if(conversionTable[token]) {
                        tokens.splice.apply(tokens, [i, 1].concat(token.replace(token, conversionTable[token]).split(/\W+/)));

                        // reset the tokens counter
                        // NOTE: This may not be ideal, the idea is that messing with the array
                        // could affect looping over it. Need to find out if theres ever going to 
                        // be a case where a rule conversion could result in another contraction which needs to
                        // be expanded, if that is the case, we need this.
                        i = 0;
                        var num_tokens = tokens.length;
                }
                // Apply the rules
                else { 
                        for(var r = 0 ; r < rule_count ; r++) {
                                var rule = rules[r];
                                if(token.match(rule.regex)) {
                                        tokens.splice.apply(tokens, [i, 1].concat(token.replace(rule.regex, rule.output).split(/\W+/)));

                                        // reset the tokens counter
                                        // NOTE: This may not be ideal, the idea is that messing with the array
                                        // could affect looping over it. Need to find out if theres ever going to 
                                        // be a case where a rule conversion could result in another contraction which needs to
                                        // be expanded, if that is the case, we need this.
                                        i = 0;
                                        var num_tokens = tokens.length;
                                }
                        }
                }
        }

        return tokens;
};





// export the relevant stuff.
exports.normalize_tokens = normalize_tokens;
