var natural = require("natural");
var wordnet = new natural.WordNet();
var async = require("async")

//add synonym and remove verbs
function remove(words, cb) {

  var resulting_words = {};

  //remove words that are not noun
  async.each(words, function(w, cb) {
    wordnet.lookup(w, function(different_meanings) {
      // console.log(different_meanings);
      different_meanings.every(function(e) {
        // console.log(e);
        if (e.pos === "n") {
          resulting_words[w] = 1;
          //console.log("resulting_words",resulting_words);
          // e.synonyms.forEach(function(syn){
          //   resulting_words.push(syn);
          // });
          return false;
        }
        else {
          return true;
        }
      });
      //add synonyms
      var tmpwords = Object.keys(resulting_words);
      async.each(tmpwords, function(tmpword, cb){
        wordnet.lookup(tmpword, function(different_meanings) {
          for (var j = 0; j < different_meanings.length; ++j) {
            different_meanings[j].synonyms.forEach(function(s) {
              resulting_words[s] = true;
            });
          }
          cb();
        });
      }, function(){
        cb();
      });
    });
  }, function() {
      cb(null,Object.keys(resulting_words));
  });
}
remove(["hello","run"], function(err,resulting_words){
  console.log(resulting_words[6]);
});