rdfstore = require "rdfstore"
async = require "async"
_ = require "lodash"

request = require("request")
updater = require("./updater")

# test = (node, req, res)->  
#   request.post "http://wordnet.rkbexplorer.com/sparql/",
#     form:
#       query: """
#       PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#       PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
# 
#       SELECT ?noun 
#       WHERE { 
#         ?noun a wordnet:NounSynset .
#         ?noun rdfs:label "investment"
#       }
#       """
#       format: "json"
#   , (err, response) ->
#     console.log response.body
#     result = JSON.parse response.body
#     nouns = _.pluck result.results.bindings, "noun"
#     nouns = _.pluck nouns, "value"
#     console.log nouns 
#     # if nouns.length > 0
#       
# test()
#     
class LDVStore
  constructor: ->
    # @store = new rdfstore.Store(
    #   persistent: true
    #   engine: "mongodb"
    #   name: "ldvstore" # quads in MongoDB will be stored in a DB named myappstore
    #   overwrite: true # delete all the data already present in the MongoDB server
    #   mongoDomain: "localhost" # location of the MongoDB instance, localhost by default
    #   mongoPort: 27017 # port where the MongoDB server is running, 27017 by default
    # , (store) ->
    #   # store.clear()
    #   #console.log "clear"
    #   # #console.log arguments...
    # )
    @store = new rdfstore.Store(
      persistent: true
      engine: "mongodb"
      name: "app18760637" # quads in MongoDB will be stored in a DB named myappstore
      # overwrite: true # delete all the data already present in the MongoDB server
      mongoDomain: "chatsey:chatsey3333@paulo.mongohq.com" # location of the MongoDB instance, localhost by default
      mongoPort: 10096 # port where the MongoDB server is running, 27017 by default
    , (store) =>
      # request.post "http://wordnet.rkbexplorer.com/sparql/",
#         form:
#           query: """
#           PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#           PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
# 
#           SELECT ?noun
#           WHERE { 
#             ?noun a wordnet:NounSynset .
#             ?noun rdfs:label "investment" .
#           }
# 
#           SELECT ?syn
#           WHERE { 
#             ?noun a wordnet:NounSynset .
#             ?noun rdfs:label "investment" .
#             ?noun wordnet:containsWordSense ?syn .
#           }
#           """
#           format: "json"
#       , (err, response) =>
#         console.log response.body
        # @store.load "text/rdf+xml", response.body, ->
        #   console.log arguments...
        # console.log response.body
        # result = JSON.parse response.body
        # nouns = _.pluck result.results.bindings, "noun"
        # nouns = _.pluck nouns, "value"
        # console.log nouns 
        # if nouns.length > 0
        
      # @store.load "application/rdf+xml", "http://wordnet.rkbexplorer.com/data/synset-investment-noun-5", ->
      #   console.log arguments...
      # @store.execute """
#         PREFIX foaf: <http://xmlns.com/foaf/0.1/>
#         PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#         PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
#         PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
# 
#         LOAD <http://wordnet.rkbexplorer.com/data/synset-investment-noun-5>
#       """, ->
#         console.log arguments...
      console.log "insert ontology"
      @insertOntoloy =>
      # store.clear()
      #console.log "clear"
      # #console.log arguments...
    )
  insertOntoloy: (cb=->)->
    try
      @store.execute """
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
        PREFIX chrn: <http://rdf.chatsey.com/resource/NounSynset/>
        PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
        PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
    
        DELETE DATA
        {
          chop:metaphone a rdf:Property .
          chop:metaphone rdfs:label "metaphone"@en .
          chop:metaphone rdfs:comment "A metaphone of wordnet:NounSynset"@en .
          chop:metaphone rdfs:domain wordnet:NounSynset .
          chop:metaphone rdfs:range rdfs:Literal .          
        }
      """, =>
        console.log arguments...
        @store.execute """
          PREFIX foaf: <http://xmlns.com/foaf/0.1/>
          PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
          PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
          PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
          PREFIX chrn: <http://rdf.chatsey.com/resource/NounSynset/>
          PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
          PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
      
          INSERT DATA
          {
            chop:metaphone a rdf:Property .
            chop:metaphone rdfs:label "metaphone"@en .
            chop:metaphone rdfs:comment "A metaphone of wordnet:NounSynset"@en .
            chop:metaphone rdfs:domain wordnet:NounSynset .
            chop:metaphone rdfs:range rdfs:Literal .
          }
        """, ->
          console.log arguments...
          cb null
    catch
      console.log arguments...
      cb null
    return
  insert: (username, statusToken, cb=->)->
    # console.log "INSERT"
    try
      @store.execute """
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
        PREFIX chrn: <http://rdf.chatsey.com/resource/NounSynset/>
        PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
        PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
        DELETE DATA
        {
          chrp:#{username} a foaf:Person .
          chrp:#{username} foaf:topic_interest chrn:#{statusToken} .
          chrp:#{username} rdfs:label "#{username}"@en .
          chrn:#{statusToken} a wordnet:NounSynset .
          chrn:#{statusToken} chop:metaphone "#{statusToken}"@en .
        }
      """, =>
        console.log arguments...
        @store.execute """
          PREFIX foaf: <http://xmlns.com/foaf/0.1/>
          PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
          PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
          PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
          PREFIX chrn: <http://rdf.chatsey.com/resource/NounSynset/>
          PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
          PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
        
          INSERT DATA
          {
            chrp:#{username} a foaf:Person .
            chrp:#{username} foaf:topic_interest chrn:#{statusToken} .
            chrp:#{username} rdfs:label "#{username}"@en .
            chrn:#{statusToken} a wordnet:NounSynset .
            chrn:#{statusToken} chop:metaphone "#{statusToken}"@en .
          } 
        """, ->
          console.log arguments...
          cb null
    catch
      cb null
    return
  findTokensForUser: (username, cb=->)->
    #console.log username
    try
      @store.execute """
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
        PREFIX chrn: <http://rdf.chatsey.com/resource/NounSynset/>
        PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
        PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
      
        SELECT ?token WHERE {
          chrp:#{username} foaf:topic_interest ?node .
          ?node chop:metaphone ?token .
        }
      """, (suc, data)->
        #console.log "FIND TOKEN", arguments...
        if not suc
          cb true
          return
        tokens = []
        data.forEach (item)->
          tokens.push item.token.value
        cb null, tokens
    catch
      cb null, []
    return
  findMatch: (username, cb=->)->
    hist = {}
    @findTokensForUser username, (err, tokens)=>
      async.each tokens, (token, cb)=>
        @findMatchSingleToken username, token, (err, usernames)->
          #console.log "usernames", usernames
          for username in usernames
            hist[username]?=0
            hist[username]++
          cb()
      , =>
        users = []
        for username, score of hist
          users.push
            username: username
            score: score
        users = users.sort (a,b)->
          b.score - a.score
        #users = _.pluck users, "username"
        #console.log "users", users
        cb null, users
  findLvl1: (username, noun, statusToken, cb=->)->
    #console.log statusToken
    @store.execute """
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
      PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
      PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
  
      SELECT ?username WHERE {
        ?node foaf:topic_interest ?noun .
        ?node rdfs:label ?username .
        ?noun chop:metaphone "#{statusToken}"@en .
        FILTER (?username != "#{username}")
      }
    """, (suc, data)->
      if not suc
        cb true
        return
      #console.log "FIND MATCH", arguments...       
      usernames = []
      data.forEach (item)->
        usernames.push item.username.value
      cb null, usernames
    return
  findLvl2: (username, noun, statusToken, cb=->)->
    #console.log statusToken
    @store.execute """
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
      PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
      PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
  
      SELECT ?username WHERE {
        ?node foaf:topic_interest ?noun1 .
        ?node rdfs:label ?username .
        ?noun1 wordnet:containsWordSense ?noun2
        ?noun2 chop:metaphone "#{statusToken}"@en .
        FILTER (?username != "#{username}"@en)
      }
    """, (suc, data)->
      if not suc
        cb true
        return
      #console.log "FIND MATCH", arguments...       
      usernames = []
      data.forEach (item)->
        usernames.push item.username.value
      cb null, usernames
    return
  findMatchSingleToken: (username, statusToken, cb=->)->
    #console.log statusToken
    @store.execute """
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX chrp: <http://rdf.chatsey.com/resource/Person/>
      PREFIX chop: <http://rdf.chatsey.com/ontology/property/>
      PREFIX wordnet: <http://www.w3.org/2006/03/wn/wn20/schema/>
  
      SELECT ?username WHERE {
        ?node foaf:topic_interest ?noun .
        ?node rdfs:label ?username .
        ?noun chop:metaphone "#{statusToken}"@en .
        FILTER (?username != "#{username}"@en)
      }
    """, (suc, data)->
      if not suc
        cb true
        return
      #console.log "FIND MATCH", arguments...       
      usernames = []
      data.forEach (item)->
        usernames.push item.username.value
      cb null, usernames
    return

module.exports = new LDVStore()